#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_config import cfg

from neutron._i18n import _

DB_OPTS = [
    cfg.BoolOpt('cern_relaxed_segment_validation', default=False,
                help=_("Use CERN-specific segment validation "
                       ", allowing multiple segments per subnet.")),
    cfg.BoolOpt('cern_subnet_pair_selection', default=False,
                help=_("Use CERN-specific V4-V6 subnet selection, "
                       "with most available IPs.")),
]

# Parameters for the Linuxbridge to OVN migration
LXB_OVN_MIGRATION_OPTS = [
    cfg.BoolOpt('ignore_lxb_port_status_on_boot', default=False,
                help=_("Skip setting the status of ports based on OVN"
                       "information on startup when we suspect they are bound"
                       "to LinuxBridge hypervisor."))
]

CERN_GROUP = 'cern'
LXB_OVN_MIGRATION_GROUP = 'lxb_ovn'

def register_cern_opts(conf=cfg.CONF):
    conf.register_opts(DB_OPTS, group=CERN_GROUP)
    conf.register_opts(LXB_OVN_MIGRATION_OPTS, group=LXB_OVN_MIGRATION_GROUP)
